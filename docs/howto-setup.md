# How-to Setup

This how-to describes how to create the OpenShift cluster in Azure using this Terraform project.

- [Hiho](#hiho)


## Install Azure CLI

Azure CLI can be installed with one command see [Install the Azure CLI on Linux | Microsoft Learn](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=script). After this we can use the Azure CLI with command `az`.

```bash
curl -L https://aka.ms/InstallAzureCli | bash
```

## Login

Execute the command `az login --tenant YYYYYYYY-YYYY-YYYY-YYYY-YYYYYYYYYYYY`. This will open browser so you can login.

<details>
  <summary>Show me</summary>

```bash
ostraaten@storm2:~$ az login --tenant b9fec68c-c92d-461e-9a97-3d03a0f18b82
A web browser has been opened at https://login.microsoftonline.com/b9fec68c-c92d-461e-9a97-3d03a0f18b82/oauth2/v2.0/authorize. Please continue the login in the web browser. If no web browser is available or if the web browser fails to open, use device code flow with `az login --use-device-code`.
[
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "b9fec68c-c92d-461e-9a97-3d03a0f18b82",
    "id": "9c5662d6-ada0-4465-a99a-410c66b69c15",
    "isDefault": true,
    "managedByTenants": [],
    "name": "NL-300000000030530-SEDC",
    "state": "Enabled",
    "tenantId": "b9fec68c-c92d-461e-9a97-3d03a0f18b82",
    "user": {
      "name": "onno.van.der.straaten@cgi.com",
      "type": "user"
    }
  }
]
```

</details>

### Public DNS

[Tutorial: Host your domain in Azure DNS | Microsoft Learn](https://learn.microsoft.com/en-us/azure/dns/dns-delegate-domain-azure-dns).


|Property               |Value               |Comment|
|-----------------------|--------------------|-------|
|Subscription           |Azure subscription 1|
|Resource group         |c2-ocp              |
|Name                   |ocp.c2platform.org  |
|Resource group location|West Europe         |

